+++
author = "Raghu Rajagopalan"
date = "2019-11-23T11:42:24.641538+05:30"
draft = false
title = "Putting a Raspberry Pi to work"
tags = ["raspberrypi", "sysadmin", "homelab"]
+++

:icons: font
:linkattrs:
:experimental:

Like a lot of fellow geeks, I have collected a few raspberry pi's at home. For the longest time, these were
basically bought and then were forgotten on a desk. At some point though, I finally got around to 
putting them to work for adblocking - and since then, they've become pretty much my goto machines when
I need a server that's available 24x7. Speaking to my friends, a lot are in the same quandry - got a 
pi, booted it up a few times and then it lies.... so here's a list of my projects

Current::
. Ad blocking with https://pi-hole.net/[Pi-Hole]
. link:/2019/11/14/theres-no-place-like-127.0.0.1-connect-back-home-with-wireguard-ipv6-and-nat/[Wireguard
VPN server] - Secure Remote connectivity to home network when travelling.
. https://github.com/raghur/gitlab-cf-le-autossl[SSL cert renewal for Gitlab
pages] - for static sites hosted on Gitlab with DNS01 challenge using
CloudFlare's API
. https://syncthing.net/[Syncthing server] - Secure, private file syncing
across machines. Note that this isn't the only syncthing server - there's a
couple of peers on the cloud  - but those are throwaway.
. Reminders server using `atd` that sends reminders over Telegram -Using
https://github.com/raghur/hey.py[hey.py]
. CUPS Print Server for Lan wide wireless printing - The initial version was
running on my link:/notes/printserver-on-tomato/[router which runs FreshTomato]
. The router is way too slow to handle print duties, so I've moved it to a RPI.
. IoT - MQTT server (Mosquitto), NRF bridge server etc 'Internet'-less IoT.
Basically, smart lights and switches using
https://github.com/arendst/Tasmota[Sonoff Tasmota] where no data ever goes out
of my router.
. https://www.minetest.net/[Minetest server] with https://redis.io[Redis] backend - Minetest is an open source voxel game engine.
    - I'll probably write this up separately - but for now I'm having way too much fun with cooperative multiplayer mode with the kids. Minetest default can be run on the pi (I'm using a Pi4 2GB) but it was being pegged once all of us joined in - likely due to the sqlite3 default backend. Redis being in-memory, offers excellent performance - the downside being that you do have to build it on your own from source.
Planned::
. link:/notes/commento/[Hosting comments for this site] - currently this runs off a slow Azure server - but I might as well run it on a RPi

Discarded::
. https://k3s.io/[Single node kubernetes cluster using K3S] - Even a Pi4 was a little iffy for this and I do have a 
decent desktop. So I've moved the K3s to my desktop - esp since I'm not using this for anything that needs to run 24x7

