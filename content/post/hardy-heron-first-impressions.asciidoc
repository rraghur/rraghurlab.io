+++
lastmod = "2013-08-21T17:14:35+05:30"
aliases = [ "/2008/03/hardy-heron-first-impressions.html",]
publishdate = "2008-03-31T17:18:00+05:30"
date = "2008-03-31T17:18:00+05:30"
title = "Hardy heron - first impressions"
id = "3301913104587062397"
tags = [ "Linux", "Tips", "Troubleshooting",]

+++
He he :-) - finally got Ubuntu Hardy heron beta on my home and work
laptop. first impressions below:

1. Wubi install from within windows is easy and works great. If after
setting up so many boxes, I can go on and on about it, I'm sure that its
great help for anyone who's on Windoze. I mean, the barrier to entry has
never gone down so much.

2. I guess once you've installed via Wubi and configured your system to
your liking, you can uninstall and take an image that you finall install
to a dedicated partition - isn't that just awesome.

3. Comes installed with Firefox 3b4 -which is awesome. Given that FF
crashes badly on yahoo, this might be a bummer for many people. Should
probably have some first time customization that will let you install
Opera.

4. Installation is super fast - took about 10 mins for wubi to install,
reboot once, finish installation and reboot again. Grub default to Last
selected would probably be a better idea.

The not so good

1. Wifi doesnt work out of the box - didn't on my Dell Inspiron 1501 or
the Dell Latitude D620. Its the ye olde broadcom problem. This is really
the BIGGEST turn off. Hope it will get fixed by the time the final
release is out. Meanwhile, had to jump through hoops getting ndiswrapper
in. I didn't go the broadcom fwcutter way since that only allows a
802.11b connection from what I read. I'm still not sure what fixed the
issue - irrespective, I had to update the system and then things started
working like a charm.

2. Compiz configuration isnt installed by default. If this is your first
time on Ubuntu and you've come this way to see the awesome 3D desktop,
then this is a bummer. Finding out what you need to do is a pain too.

I think that's all there is to it. Its great once wifi starts working
normally.

