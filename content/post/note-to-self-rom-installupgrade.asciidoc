+++
lastmod = "2013-08-21T17:14:35+05:30"
publishdate = "2011-07-28T16:08:00+05:30"
aliases = [ "/2011/07/note-to-self-rom-installupgrade.html",]
date = "2011-07-28T16:08:00+05:30"
title = "Note to self: ROM install/upgrade"
id = "1778149954549955237"
tags = ["android", "mobile"]

+++

- nandroid backup - amon ra recovery
- Reboot recovery, install zip
- Install Link2SD-preinstall.zip (only on cyanogen based ROMs)
- Boot
- Play around...make sure things work.
- Install other niceties/Troubleshoot
    1.  Link2SD - database error.Just uninstall and reinstall.
    2.  /etc/gps.conf - change to sg.pool.ntp.org
- Charge to 100%
- Reboot into recovery
- wipe battery stats
- reboot
- Run down the battery
- Recharge to 100%

