+++
lastmod = "2013-08-21T17:14:34+05:30"
publishdate = "2004-08-28T05:52:00+05:30"
aliases = [ "/2004/08/perl-you-either-love-it-or-you-hate-it.html",]
date = "2004-08-28T05:52:00+05:30"
title = "Perl - you'll either love it or you'll hate it, but you won't ignore it if you want to get things done!"
id = "7546114028295559136"
tags = ["perl", "python", "programming"]
slug = "Perl-youll-either-love-it-or-youll-hate-it-but-you-wont-ignore-it"

+++

Have spent the better part of yesterday night writing a Perl script to
aid my fiance' in doing some log analysis. And I'm no Perl guru - my
biggest Perl program till date wouldn't have been more than a 100
lines.
Here's my raves and rants on it 
Raves 

1.  Most powerful reg ex capabilities with a full programming language
to back it up!
2.  Large and powerful standard library.
3.  Platform independent. Have windoze at work, usually am on a cygwin
console and have linux/windows dual boot at home. Perl doesn't complain!

 
Rants 

1.  Syntax - I've never got used to the ultra concise Perl vars. But
then, I dont use it regularly enough. Still, the syntax just isnt
readable.
2.  Syntax isn't orthogonal - guess what I'm trying to say is that there
isn't one and only one way to achieve something. While this is totally 
against the Perl mantra of "There's more than one way to do it", in
spite of using it on and off, I've never been able to embrace 
1.  References
2.  Subs
3.  OO Perl
4.  Writing perl mods

 
I've used all of them at sometime or the other but the next time I pick
up Perl to do some nifty little script, I find myself going through
perlsub, perlref, perlreftut and perltoot - not nice!

 
I guess at sometime I did come across Python - which is supposed to
address all the shortcomings of Perl while retaining the pluses. But
then once I got everything set up, saw that its indentation sensitive!!
Now does that remind me of something....called FORTRAN ??

Anyway, it put me off so much that I never took a look at it again -
probably I should, and be more generous while at it!
Meanwhile, I'll use Perl the next time I need to write a nifty little
script to do some text analysis. 

Interested? - I'll recommend
link:www.sthomas.net/roberts-perl-tutorial.htm[Robert's perl tutorial]. 
 
*Programmer*: _n._ Person who spends 10 hours automating something that
takes an hour manually just so that

1.  He can see his program complete the work in 1 hour.
2.  He can tell his poor kith and kin what a great guy he is.

