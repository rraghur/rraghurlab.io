+++
lastmod = "2005-03-10T12:31:52+05:30"
publishdate = "2005-03-07T10:18:00+05:30"
aliases = [ "/2005/03/sharepoint-web-services-and-ntlm.html",]
date = "2005-03-07T10:18:00+05:30"
title = "Sharepoint Web Services and NTLM Authentication"
id = "111017093157370630"
tags = ["sharepoint", "webservices"]
+++

I noticed this strange behaviour and thought its worth a post - till I
find a solution/explanation.

Scenario: I have a custom web service running under sharepoint. This
means that theÂ .asmx is in "c:\Program Files\Common Files\Microsoft
Shared\web server extensions\60\ISAPI" and the binaries in the bin
folder under it. The url of the web service is

http://aditi277/XTeam/spaces/outlooksynch/_vti_bin/OfflineServices.asmx 

The _vti_bin has NTLM authentication enabled.

When I paste the url in a browser, I go straight through to the page
without any authentication! (I've set "Prompt for username and password"
in IE).

In the web service, Context.Request.IsAuthenticated returns False and
Context.User.Identity.Name returns an empty string. This is as expected.

I have a small program that connects to the web service. In the program
I'm setting the credentials for the web service.

The fun starts when I create an SPSite object in the web service code.
The SPWeb.CurrentUser.ToString() returns the username of the credentials
set on the client!!! How the heck is the identity being passed on to
Sharepoint? Or is it a bug in sharepoint that it doesnt set the Context
properties? I'm stumped!!!


