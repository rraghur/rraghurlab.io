+++
lastmod = "2013-08-21T17:14:35+05:30"
aliases = [ "/2010/01/a-new-tool-for-toolbox.html",]
publishdate = "2010-01-30T14:40:00+05:30"
date = "2010-01-30T14:40:00+05:30"
title = "A new tool for the toolbox!"
id = "7464950811878785755"
tags = [ "Linux", "Tips", "Tools", "Utilities",]

+++
Firstly - my VM setup:

I'm running http://www.virtualbox.org[Virtualbox] with Xubuntu 9.10 on
Win7 host - and its pretty. Its on a office standard issue Dell D531 -
meaning they're AMD Turion X2 TL-60 and 2GB of RAM.

Now the Turion's supposed to have
http://en.wikipedia.org/wiki/List_of_AMD_Turion_microprocessors#Turion_64_X2_.2F_Turion_X2[hw
virtualization (AMD-V)] however, the moment hw virtualization was
enabled in virtualbox and I tried starting the vm, the machine would
hard reboot!!!

After searching high and low, turns out that its an issue with Dell
bioses and they dont have any updates. Here's a
http://social.technet.microsoft.com/Forums/en/w7itprovirt/thread/85d3ca80-7729-43b5-82f1-c29f6f05f0a1[page
that tracks the issue]. Imagine my happiness when a couple of days ago,
found that dell had released an unofficial bios update (T12).Well, its
gone in, and things are running swimmingly well - my VM now has 2 procs,
is stable and I hardly feel I'm in a VM :). In fact, this post is coming
from the VM  - firefox with 12 tabs, a few terminals and emacs running
on 600 MB of RAM.

Now let me come to the new tool I was talking about

I like to run the VM full screen - feels best that way. After trying out
enough and more of multiple desktop softwares, have finally settled on
http://virtuawin.sourceforge.net/[VirtuaWin] - beats the crap out of
other tools, systray integration is great, has window rules and so on.
Over the past couple of weeks, its come close to the ideal tool - does
the job well and you hardly know its there :-)

