+++
lastmod = "2013-08-21T17:14:35+05:30"
aliases = [ "/2008/04/firefox-3-beta-5-released-yahoo-mail-is.html",]
publishdate = "2008-04-02T19:32:00+05:30"
date = "2008-04-02T19:32:00+05:30"
title = "Firefox 3 beta 5 released. Yahoo Mail is still broken."
id = "1140012169145334001"
tags = [ "Firefox", "Web",]

+++
Firefox 3 Beta 5 release today. Release notes and downloads
http://en-us.www.mozilla.com/en-US/firefox/3.0b5/releasenotes/[here].

Installed it as soon as I got to know today morning and the first thing
to check was whether Yahoo Mail still crashed. Initially, Yahoo Mail
seemed to work alright for all of 50 seconds - quickly moving over items
in inbox caused Firefox to crash :-(

Guess will wait for some more time. I'm sure there's a bug report
somewhere on this - Yahoo mail was broken on Beta 2, got fixed in Beta
3, then was broken in Beta 4  and is still broken on Beta 5.

Will wait for it to be fixed - Any idea if this is a firefox issue or a
Yahoo! issue? Seems odd that script can cause the browser to crash so
badly.

