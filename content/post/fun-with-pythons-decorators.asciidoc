+++
lastmod = "2013-08-21T17:14:35+05:30"
aliases = [ "/2010/05/fun-with-pythons-decorators.html",]
publishdate = "2010-05-28T02:42:00+05:30"
date = "2010-05-28T02:42:00+05:30"
title = "Fun with python’s decorators"
id = "8240754468878811007"
tags = [ "code", "decorators", "Python",]

+++
Was in need of a utility function that can retry an arbitrary function a
few times before giving up. Essentially something like Gmail or Google
Readers behavior when there’s no network connection.

Thought it would be a few minutes job to cook up a decorator utility in
Python. Boy! was I wrong! I mean, the basic use case is definitely
trivially easy with Python – however, once you want something that’s
more useful than that and resembles something that you’d actually use in
production, the complexity goes over the top!

Anyway, I’m figuring out all sorts of fun things about decorators – and
all of it the hard way! OTOH, its a  lot of fun to write small test code
to test & validate assumptions!

Make no mistake – I’m still a python fanboy :) – just that going through
some pains with decorators right now. Will follow this up with a
longer/detailed post that may have some useful insights I’ve gained till
then. Thanks for stopping by!

