+++
lastmod = "2013-08-21T17:14:34+05:30"
aliases = [ "/2007/02/imaging-tenth-dimension.html",]
publishdate = "2007-02-24T22:11:00+05:30"
date = "2007-02-24T22:11:00+05:30"
title = "Imaging the tenth dimension"
id = "5713300678864684994"
tags = [ "Physics", "stumble", "Very Cool",]

+++
http://www.tenthdimension.com/medialinks.php[Amazing flash movie!]

Great flash movie explaining dimensions 4 (time), 5, 6 all the way uptil
10. And why they stop after 10!

I've never come across a clearer explanation ever and the great
visualization does the trick.

