+++
author = "Raghu Rajagopalan"
date = "2017-06-06T18:27:27+05:30"
description = "Getting a working cross platform experience with Git for Windows, MSYS git and Linux just wants me to jump off a cliff!"
tags = ["git", "rant", "windows"]
title = "MSYS2 Git, Git for Windows - line endings and cross platform setup"
type = "post"

+++

I use multiple machines - work machine is Windows 8.1 with Git for Windows and
MSYS Git while at home I have Linux. Given that I frequently pull and push from
any of these depending on what I'm using at any given moment. Having a setup
that works with least surprises is an important quality of life factor. If I
can, I prefer linux versions since I can share the same setup with my home
machine and it's easier for muscle memory.

Today, given that I have finally moved from Screen to tmux, I decided to give
things a go on MSYS2 on my work laptop. If you're a git user, then you know
where this is heading - what followed is probably 2 hours wasted trying to
figure out the right incantation of `core.autocrlf`, `.gitattributes` and
`core.eol` to retain my sanity.

So, for the uninitiated (though I doubt anyone here would fall into that
group), MSYS2 git by default will do line ending translation. This means that
if a file is in the repo with LF, when it checks out, it will be modified to
 have CRLF as line endings. What this results in is that under MSYS2 `git
 status` reports every file as modified.

[source,shell]
----
On branch master
Your branch is ahead of 'origin/master' by 2 commits.
  (use "git push" to publish your local commits)
Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git checkout -- <file>..." to discard changes in working directory)

        modified:   .agignore
        modified:   .config/openbox/lubuntu-rc.xml
        modified:   .conkyrc
        modified:   .ctags
        ....
        ....
        # goes on for pretty much every file
----

and if you were to check `git diff ` you'd just find every line with a `^M`
showing up as a diff. If you were to commit this, it'd be terrible since every
line will show up as a diff.

Now, admittedly, this is a problem if you want MSYS2, Git for Windows and Linux
- but the easiest solution I found was to just junk CRLF altogether. Most
editors nowadays are end of line aware barring stupid Notepad, but then I'm not
going to be using notepad anytime soon.

To turn off all linefeed translation do this

[source,shell]
----
git config --global core.autocrlf input
----

And if you ever want to enable it for a repository locally, you can always do
`git config core.autocrlf true`.

So that's about all I'm going to do about it. Note that setting
`autocrlf=input` does not fix files that are already in the repository. If
you'd like to normalize all line endings, then you have do quite a bit more
work. I'm not the only one who thinks this is a complete PITA - just see
https://stackoverflow.com/questions/2517190/how-do-i-force-git-to-use-lf-instead-of-crlf-under-windows[this
question at SO^] - asked 7 years ago, a 100K views and the selected answer
doesn't even have the most upvotes! :)

I'm still waiting to get Windows 10 on my work laptop. I'm hoping with ubuntu
on WSL I would'nt need to jump through all these hoops.
