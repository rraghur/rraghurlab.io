+++
author = "Raghu Rajagopalan"
date = "2018-06-12T14:40:02.054304+05:30"
draft = false
title = "Programming the LEGO Mindstorms EV3 in Linux"
tags = ["linux", "virtualbox", "lego-mindstorms-ev3"]
heroImage = "/img/legos-unsplash.jpg"
+++

We got the kids a Lego Mindstorms set last week. I've been eyeing it for quite a
few years, but have been waiting for the kids to grow up a bit till they can
handle it - especially since LEGOs are costly in general and the Mindstorms
kit in particular costs a pretty penny.

image:https://images-na.ssl-images-amazon.com/images/I/61JuryQt%2BQL.jpg[mindstorms]

Anyway, they've been having a blast building our first robot - for now with
LEGO's android app over a Bluetooth connection. The android app is
convenient but lacks a few features and so on. So, today I set about setting up
the desktop app.

LEGO's desktop app only works on Windows or Mac. I don't have either - it's
Linux only at home. Also, Wine isn't an option since WineHQ lists the LEGO app
as 'Garbage'. That left virtualbox - I have a Windows VM just for such
occasions. However, getting USB connection to be passed through to the guest
required some fiddling about.

== The problem

With Virtualbox running with USB controller turned on, I just didn't see any of
the USB devices connected to the host. As it so often happens, a long time ago
I'd set all this up to be able to flash OS images on my then Redmi phone.. so
the VM had a bunch of USB filters and so on. Somehow memory tricked me to
overlooking the fact that I wasn't able to see USB devices connected on the
host.

After installing the LEGO app, I connected the ev3 brick and it just wouldn't
show up on the guest. At first, I assumed (wrongly) that this was
something to do with the guest since in Windows was constantly failing to
find USB host controller drivers.

== The Solution

Eventually, it clicked that irrespective of the guest, virtualbox on the host
just wasn't seeing the USB devices. One piece of advice on the internets was to
ensure that my user was part of the `vboxusers` group. This I did with

```
sudo adduser $USER vboxusers

# https://superuser.com/a/957636
```

Now, this alone did not help. Turns out that the user you're running as needs
access to `/dev/bus/usb/` device tree. In my case, everything under that is
owned by `user=root` and `group=root`. Turns out that we also need to tweak the
udev rules that come with virtualbox.

. First get the GID of the `vboxusers` group from `/etc/group`
. Edit `/etc/udev/rules.d/60-vboxdrv.rules`
. Add the GID to the lines below as shown (`999` is the group id in the example)
+
```
SUBSYSTEM=="usb_device", ACTION=="add", RUN+="/usr/local/VBoxCreateUSBNode.sh $major $minor $attr{bDeviceClass} 999"
SUBSYSTEM=="usb", ACTION=="add", ENV{DEVTYPE}=="usb_device", RUN+="/usr/local/VBoxCreateUSBNode.sh $major $minor $attr{bDeviceClass} 999"
```

Give it a quick restart... post that you should find that:

. Plugging in a usb device should populate `/dev/vboxusb` tree and underneath
that, the group should be `vboxusers`
. `vboxmanage list usbhost` should show you a list of devices.

After this, Windows found the EV3 brick and installed drivers and after that
I'm able to use the LEGO app to program. Time to teach the kids now :)

== Next stop

I'm probably going to put Debian Linux via the https://ev3dev.org[ev3dev]
firmware on the brick once I've exhausted the basics with the kids - and then
probably show them the ropes with Python. But there's a lot of time for that -
still lots to explore.
