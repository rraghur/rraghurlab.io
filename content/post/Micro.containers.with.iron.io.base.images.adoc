+++
author = "Raghu Rajagopalan"
date = "2017-05-23T12:27:56+05:30"
publishdate = "2017-05-23T12:27:56+05:30"
lastmod = "2017-05-23T13:29:59+05:30"
draft = false
title = "Microcontainers with Iron.io's base images"
tags = ["docker"]
+++


While I knew about http://alpinelinux.org[Alpine Linux^] and have been using them to create smaller docker images,
I just came across Iron.io's set of https://hub.docker.com/u/iron/[alpine based container images] for pretty much every major programming language.

If you haven't come across Alpine yet, it's a tiny (5mb) security focused linux distribution based on musl libc & busybox.
What's nicer is that they also have dev images which allow you to use images for development workflows as well.

For ex, my https://hub.docker.com/r/rraghur/hugo-asciidoctor/[`rraghur/hugo-asciidoctor:alpine`] image based on https://hub.docker.com/r/iron/ruby/[`iron/ruby`] went from 176Mb to 88Mb! Good Job!  icon:thumbs-o-up[2x,flip=horizontal]


