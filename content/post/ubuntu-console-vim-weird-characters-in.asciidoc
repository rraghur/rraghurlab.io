+++
lastmod = "2013-08-21T17:14:35+05:30"
aliases = [ "/2012/01/ubuntu-console-vim-weird-characters-in.html",]
publishdate = "2012-01-16T13:53:00+05:30"
date = "2012-01-16T13:53:00+05:30"
title = "Ubuntu, Console VIM - weird characters in insert mode"
id = "5186557571766591056"
tags = [ "Linux", "Tips", "Troubleshooting", "ubuntu", "vim",]

+++
Now that I feel quite comfy with VIM, over the weekend I needed to edit
a config file in my Ubuntu 10.10 Virtualbox machine quickly. Instead of
GVim, I just opened the file in console VIM. As I hit i to get into
insert mode, a bunch of weird character boxes were inserted. That was
not good at all :( - just when you think you're comfortable with
something if it does something totally weird. In any case, I was in too
much of a hurry to bother and went about editing my file with gVim.
Also, backspace was wonky (same weird characters) - so I felt better.
For some reason that I fail to understand, why must Linux make proper
backspace and delete handling such a pain! In any case, it's something
that I've dealt with enough times to know that there'll be something on
Google.



Later on, tried to see what all the fuss was about. Googling around, I
found `:help :fixdel` and that seemed simple enough. Alas, when I tried
it out, it didn't fix the issue at all. Also, I seemed to be getting
weird characters just pressing `i` to get into insert mode - and the VIM
wiki page didn't have anything about that. Neither did Google turn up
anything that seemed related.



So today early morning, on a whim, read up a little on VIm terminal
handling. I have the following in my .vimrc

[source, text]
----
set t_Co=256
----
Maybe it was the color escape code that was coming in - so checked out
`:echoe &term` which returned `xterm` under gnome-console and
`builtin_gui` under gvim. So I've put the following bit in my .vimrc and
it seems to have fixed things nicely:

[source, text]
----
if &term == "xterm"
set term=xterm-256color
endif
----

