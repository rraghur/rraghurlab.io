+++
lastmod = "2005-03-10T18:23:37+05:30"
publishdate = "2005-03-10T18:23:00+05:30"
aliases = [ "/2005/03/am-learning-how-to-call-web-services.html",]
date = "2005-03-10T18:23:00+05:30"
title = "Calling web services asynchronously"
id = "111045921737960719"
tags = ["programming", "async"]
+++
 

Am learning how to call web services asynchronously....here are some
good resources to start off with.
http://msdn.microsoft.com/library/default.asp?url=/library/en-us/vbcon/html/vbtskcallingwebserviceasynchronously.asp[Accessing
an XML Web Service Asynchronously in Managed Code] 

http://msdn.microsoft.com/library/default.asp?url=/library/en-us/cpguide/html/cpconInvokingWebServicesAsynchronously.asp[Communicating
with XML Web Services Asynchronously] 

I would actually like to make asynch calls from vb (sic!)...well that's
quite some way off..

