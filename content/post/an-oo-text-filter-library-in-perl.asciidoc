+++
lastmod = "2013-08-21T17:14:34+05:30"
publishdate = "2004-09-01T08:21:00+05:30"
aliases = [ "/2004/08/an-oo-text-filter-library-in-perl.html",]
date = "2004-09-01T08:21:00+05:30"
title = "An OO Text filter library in Perl"
id = "2233112327385493773"
tags = ["perl", "programming"]
+++

Have put together a skeleton of a Region matching library in Perl. Idea
was triggered from the log analysis script that I was writing last week.
That worked pretty well, except for the fact that my fiance wanted to do
other little bits with some other regions of the same file. 

And then I got thinking and realized that 90% of the time I am trying to
match text delimited by some patterns and trying to do something with
the text in between - for instance, add log statements at start and end
of function calls in a VB.NET source, clean up objects at end of
function etc etc. 


....

public function f
----
----
----
.
.
----
end function

public function g
----
----
----
.
.
----
end function
....

Also, being able to string two (or more) filters one behind the 
other was another must have feature. i.e., filtered output of the first
becomes input for the second. 
And being able to negate a filter would be of added use - I guess it'll
just ease up stuff syntactically. 
So I've been spending quite some time in the past two days and have
finally the app code looks simple as I'd wanted... 

[source, perl]
----
#! /bin/perl
use strict;

use TextRegion::StartFilter;
use TextRegion::StartEndFilter;
use TextRegion::AndFilter;
my ($filter1, $filter2, $andfilter);
sub onMatchBegin; #app code callback
sub onMatchEnd; #app code callback
$| = 1;
$filter1 = new TextRegion::StartEndFilter('^\w+:\d+>\s*sysbuf', '^\**DONE\**ERRORBUF');
$filter2 = new TextRegion::StartFilter('^SWER');
$andfilter = new TextRegion::AndFilter($filter1, $filter2);
$filter2->onBeginEvent(\&onMatchBegin);
$filter2->onRegionEndEvent(\&onMatchEnd);
$andfilter->start;

while() {
  $andfilter->apply($_);
}
$andfilter->end;
----

Am I on top of the world today!!! :D :D. Actually, the fun part of 
programming is that spark of an idea that you can build and give a
tangible shape - always beats me how people can't not like programming.

