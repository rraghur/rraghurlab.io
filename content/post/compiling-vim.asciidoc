+++
lastmod = "2013-08-21T17:14:35+05:30"
aliases = [ "/2011/12/compiling-vim.html",]
publishdate = "2011-12-28T15:06:00+05:30"
date = "2011-12-28T15:06:00+05:30"
title = "Compiling VIM"
id = "5357446535797963677"
tags = [ "Linux", "Tips", "vim",]

+++
Running ubuntu 10.10 here and ubuntu repos have only vim 7.2. I'm sure
there's a ppa out there that has 7.3, but thought
that compiling vim from source would be a good exercise - plus I get to
compile it with the options that I'd like
rather than relying on someone's build.



Here's the options that I enabled:
[source, text]
----
CONF_OPT_PERL = --enable-perlinterp=dynamic
CONF_OPT_PYTHON = --enable-pythoninterp
CONF_OPT_RUBY = --enable-rubyinterp
CONF_OPT_GUI = --enable-gui=gtk2
CONF_OPT_FEAT = --with-features=huge
BINDIR = /usr/bin
DATADIR = /usr/share
----

Here's hte other dependencies I had to install
[source, bash]
----
sudo apt-get install libperl-dev ruby-dev python-dev libgtk2.0-dev
----
Once you have the deps installed, just run
[source, bash]
----
make
sudo checkinstall
----

