+++
author = "Raghu Rajagopalan"
date = "2019-06-21T10:31:34.817122+05:30"
draft = false
title = "STM32 Bluepill Dev Notes"
note_tags = ["development", "arduino", "homelab"]
type = "note"
+++

== STM32

ARM Cortex M3 dev board, many cheap clones on Aliexpress (~ 120INR), well supported, fast, low power consumption

.Pinout
[.center.text-center]
image::https://wiki.stm32duino.com/images/a/ae/Bluepillpinout.gif[alt]
== Bootloader

Chip comes with a system BL in read only ROM/silicon that allows flashing over serial port and then jumps to 
0x8000000. You could have a custom boot loader there that does additional stuff. There's a few community developed 
ones

=== Serial - in built as above, cannot be removed.
- Pins
* PA9 - TX
* PA10 - RX
- BOOT0 should be high
- Needs a USB to UART converter

=== STM32Duino/ Roger's BL/ Maple BL 2.0
- Stable, works well for me.
- Has a fastboot version that works without waiting.
- Once flashed, disable *SWD pins* so flashing over STlink won't work anymore
** FIX: Keep BOOT1 high/floating during reset so that it goes into permanent DFU mode and can be detected over STlink
- has a 5 sec wait during boot where it waits for DFU (Device Firmware Update) upload. Use the FASTBOOT version 
- Flashing
+
[source, shell]
----
STM32_Programmer.sh -c port=SWD -e all -d /data/common/stm32/BL/generic_boot20_pc13.bin  0x8000000 -v
----

=== Maple 2.0 - FASTBOOT
- Avoids 5s wait if waking up from STANDBY/SLEEP (unless BOOT1 is high)
- Flash with STM32_programmer 
+
[source, shell]
----
# flash normal bl first.
STM32_Programmer.sh -c port=SWD -e all -d /data/common/stm32/BL/generic_boot20_pc13.bin  0x8000000 -v
# then flash fastboot
STM32_Programmer.sh -c port=SWD  -d /data/common/stm32/BL/generic_boot20_pc13_fastboot.bin  0x8000000
----

=== USB HID
new one - worked a few times and then ran into trouble with it. Try again later.


=== Does BL matter?
It should not - apart from the convenience of flashing and then jumping to your
user code. But, it does - my code to check wake up reason and go into
standby/sleep didn't work without maple BL.  See
https://www.stm32duino.com/viewtopic.php?f=9&t=4705[STM32Sleep - Hang if
flashed via SWD, works if using STM32Duino bootloader]

== Cores

Cores refers to platform/foundation libs that are used to bring the board up
and then expose a Arduino API on top. THere are
http://stm32duino.com/viewtopic.php?f=42&t=97[multiple cores]


. Roger's Core/Maple Core - More stable, things generally work, has libs and
you get better forum support for blue pill.  However, with official STM core
coming in, this is unlikely to be developed further.  For blue pill, stay on
this.

. Official STM32 Arduino Core - newer, official, under development. Things
sometime work and while improving, sometimes things may not do what it says on
the tin. However, keep this under watch. Apparently has better support STM32F4.

== Problems

. USB enumeration fails - esp when using Official STM Core
* Workaround: initialize Serial1 (PA9, PA10) and write msgs to it.
+
[source, cplusplus]
----
#include "src/LibC/LibC.h"

void setup(void)
{
     Serial1.begin(9600);
     printf_setprint(&Serial1);
     // this will appear on Pa9, PA10
     printf("\r\nWUF : %lu", PWR->CSR && PWR_CSR_WUF);
     // ....
}
----
* You'll need the USB-UART adapter to connect and read from PA9/PA10 of course.
. RF24 lib didn't seem to like the official STM core much
* configuration wasn't sticking and it was showing weird values
. Could not read WUF/SBF with Official STM core


== References

. https://www.st.com/content/ccc/resource/technical/document/reference_manual/59/b9/ba/7f/11/af/43/d5/CD00171190.pdf/files/CD00171190.pdf/jcr:content/translations/en.CD00171190.pdf[Data sheet]
