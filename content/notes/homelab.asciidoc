+++
author = "Raghu Rajagopalan"
date = "2018-11-13T10:24:34.625651+05:30"
draft = false
title = "Belkin N300 Debrick notes"
note_tags = ["router", "homelab"]
type = "note"
+++

:icons: font
:linkattrs:
:experimental:
:toc:


## Belkin f7d7301 debrick story

=== USB UART - Serial recovery

. USB UART/JTAG board from Ali - CJMCU FT232H
. Default mode is USB UART
. Router pins
.. 4 pin header. Arrow marks pin 1 (VCC)
.. Pin 2 - TXD
.. PIN 3 - RXD
.. PIN 4 - GND
+
Only need to connect pin 2, 3 and 4 to pins on FT232h board. TX & RX are interchanged (TX on router goes to RX on board)
. When bricked, serial console did not show any output on power on. See recovery mode below.

==== Was it necessary

Probably not - but I doubt I'd have got the router back to life without it. Immensely helpful since it shows 
what's going on when you're trying to use the CFE miniweb. Also, I doubt I'd have tried `tftp put` unless 
I could see what was going on the serial console.


=== Belkin - recovery mode/CFE notes

. Doing a 30-30-30 reset with the _WPS button_ seemed to put some life into the router so that it came out of it's funk
and serial console printed boot messages.
. Once serial console was available, setting static IP on PC to 192.168.2.x and browsing to 192.168.2.1 shows CFE recovery page.
. Flashing from CFE web browser DID NOT WORK - just shows the browser waiting
. CFE command to boot from tftp site did not work either [tftp transfer errors out ]
. TFTP put from the PC worked consistently:
.. `tftp -i 192.168.2.1 put dd-wrt.v24-37305_NEWD-2_K2.6_mini_f7d7301.bin`
... This is msys tftp. Any linux one will do also.
.. Power on the router - it should pick up the tftp transfer and start flashing.. Sometimes takes a couple of tries.


== Firmware notes

. dd-wrt is broken horribly - *AVOID*
.. k26 - lan ports didn't work.
.. k30 - broke router.
.. does not flash properly through CFE - the only one that did was the mini build above
.. USB ext2 partitions not recognized.
. Tomato fw
.. Shibby build 140 works well - pick one that clocks in under 8MB bin file
- But printer discovery (multicast DNS) is broken on shibby 140
.. FreshTomato-MIPS works well.

== For future brick proofing

. backup CFE - `dd if=/dev/mtd0 of=/tmp/cfe.bin`
.. link to cfe - https://mega.nz/fm/9ctx2Q6D[Mega backup folder]
. Restore - if you have a cfe console, then you could probably do `flash`
- but if you need a cfe backup, then your cfe is likely borked and only a jtag restore is going to help.

