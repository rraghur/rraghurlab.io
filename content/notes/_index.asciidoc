== Notes

Most of these are related to my home infrastructure that I manage/maintain -
that includes my media server, routers, printers, a couple of servers in the
cloud and a few Linux desktops and then some other stuff like this blog and a
couple of other static sites. Also included are rants about family's Windows or
Android devices that I to occasionally troubleshoot. 

In any case, most of these are of narrow interest to anyone else and so doesn't
go on the blog - however, since I do have to go back to some of these after
many months, it helps out the future me who's scratching his head trying to
remember exactly what magic incantation was used the last time. Basically, a
section for stuff that I care about and took time to figure out that future me
will thank me for.
