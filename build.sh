#! /bin/sh
echo "Running in $(pwd)"
echo "asciidoctor options:"
cat .asciidoctor
rm -rf public
hugo | tee output.txt
find public -name 'index.json' -exec cat {} + | sed -e "s/^}/},/"  -e "1s/{/[{/" -e '$s/},/}]/' > public/content.json       
node build-index.js < public/content.json > public/lunr.idx
grep -q 'Total in' output.txt
exit $?
