// console.log("site.js called");
hljs.initHighlightingOnLoad();
$(function(){
    // console.log("ready called");
    $("body").imageview({
        targetSelector: ".imageblock img",
        srcAttr: "src",
        titleAttr: function(el) {
            // console.log("target is", el);
            return $(el).parents(".imageblock").find(".title").text()
        }
    });

});
