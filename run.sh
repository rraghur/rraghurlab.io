#! /bin/bash

HUGO_REV=${1:-0.68.3}
DOCKER_TAG="v$HUGO_REV-alpine"
podman run -it --rm -p 1313:1313 -v `pwd`:/usr/share/blog "docker.io/rraghur/hugo-asciidoctor:$DOCKER_TAG"
